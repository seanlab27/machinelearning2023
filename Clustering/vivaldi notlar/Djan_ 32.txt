Django kurulumu
sudo pacman -S python-pip
sudo pip3 install virtualenv
mkdir ~/myproject
cd ~/myproject
virtualenv env
source env/bin/activate
pip3 install django
deactivate

django-admin startproject projectname
cd projectname
./manage.py runserver
./manage.py createsuperuser
./manage.py syncdb
./manage.py migrate
Mon Jan 10 2022 02:51:20 GMT+0300 (GMT+03:00)

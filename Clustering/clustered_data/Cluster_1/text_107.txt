Naive Bayes modeli için kullanılan parametreler arasında
Olasılık dağılımları Bu parametreler verilen eğitim verileri için her sınıf etiketi için her özellik için olasılık dağılımını belirler Bu olasılık dağılımları Gaussian Naive Bayes için normal dağılım veya Multinomial Naive Bayes için sayısal değerleri temsil eden bir dağılım olabilir
Laplace smoothing Bu parametre olasılık değerlerinin  olması durumunda oluşabilecek hata oranını azaltmak için kullanılan bir tekniktir Bu parametre her özellik için verilen eğitim verileri için olasılık değerlerini hesaplarken kullanılır
Naive Bayes modeli için bir optimizasyon fonksiyonu gerekmez çünkü model eğitim aşamasında olasılık dağılımları ve Laplace smoothing parametreleri kullanılarak doğrudan hesaplanır
Ancak veri setinin özellikleri üzerinde feature selection yapmak eğitim süresini optimize etmek ve overfitting problemine karşı mücadele etmek için birçok yöntem mevcut bunlar arasında en önem




devam et
li olanlar
Cross validation  Bu yöntem veri setinin bölünmesiyle elde edilen farklı parçalar üzerinden modeli eğitmek ve test etmek suretiyle modelin genelleştirme performansının ölçülmesidir
Regularization  Overfitting problemine karşı kullanılan yöntemler arasındadır Bu yöntem ile modelin ağırlıklarının sınırlandırılması suretiyle modelin daha genel bir şekilde öğrenmesi sağlanır
Bu yöntemler Naive Bayes modelinin optimizasyonunu sağlar ve modelin performansını arttırmak için kullanılabilir
Fri Jan    GMT GMT
httpschatopenaicomchat